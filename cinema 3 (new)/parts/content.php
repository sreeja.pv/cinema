<?php
global $conn;
if($_GET['question'] === 'ex1'){
    echo '<h2> Voila, tous les clients</h2>';
    echo '<table>
            <tr>
            <th>Last Name</th>
            <th>First Name</th>
            <th>DOB</th>
            </tr>';
    connect_db();
    $reponse = $conn->query('SELECT * FROM clients');
    while ($donnees = $reponse->fetch()){
        echo '<tr>';
        echo '<td>' .  $donnees['lastName'] . '</td>';
        echo '<td>' .  $donnees['firstName'] . '</td>';
        echo '<td>' .  $donnees['birthDate'] . '</td>';
        echo '</tr>';
    }
    $reponse->closeCursor();
    echo '</table>';
}
else if($_GET['question'] === 'ex2'){
    echo '<h2>Voila, tous les types de spectacles</h2>';
    echo '<table><tr>
            <th>showTypes</th>
            </tr>';
    connect_db();
    $reponse = $conn->query('SELECT * FROM showTypes');
    while ($donnees = $reponse->fetch()){
        echo '<tr>';
        echo '<td>' . $donnees['type'] . "</td>";
        echo '</tr>';
    }
    $reponse->closeCursor();
    echo '</table>';
}
else if($_GET['question'] === 'ex3'){
    echo '<h2>Afficher les 20 premiers clients</h2>';
    echo '<table><tr>
            <th>ID</th>
            <th>Last Name</th>
            <th>First Name</th>
            </tr>';
    connect_db();
    $reponse = $conn->query('SELECT * FROM clients LIMIT 0,20');
    while ($donnees = $reponse->fetch()){
        echo '<tr>';
        echo '<td>' . $donnees['id'] . "</td>";
        echo '<td>' . $donnees['lastName'] . "</td>";
        echo '<td>' . $donnees['firstName'] . "</td>";
        echo '</tr>';
        }
        $reponse->closeCursor();
        echo '</table>';
}
else if($_GET['question'] === 'ex4'){
    echo '<h2>N afficher que les clients possédant une carte de fidélité</h2>';
    echo '<table><tr>
            <th>Last Name</th>
            <th>First Name</th>
            </tr>';
    connect_db();
    $reponse = $conn->query('SELECT * FROM clients where cardNumber IS NOT NULL');
    while ($donnees = $reponse->fetch()){
        echo '<tr>';
        echo '<td>' . $donnees['lastName'] . "</td>";
        echo '<td>' . $donnees['firstName'] . "</td>";
        echo '</tr>';
    }
    $reponse->closeCursor();
    echo '</table>';
}
else if($_GET['question'] === 'ex5'){
    echo '<h2>Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre "M"</h2>';
    echo '<table>';
    connect_db();
    $reponse = $conn->query('SELECT * FROM clients where lastName LIKE "M%" ORDER BY lastName ASC');
    while ($donnees = $reponse->fetch()){
        echo '<tr>';
        echo '<td>' . "Nom : " . $donnees['lastName'];
        echo "<br>";
        echo "Prénom : " . $donnees['firstName']. "</td>";
        echo '</tr>';
    }
    $reponse->closeCursor();
    echo '</table>';
}
else if($_GET['question'] === 'ex6'){
    echo '<h2>Afficher le titre de tous les spectacles ainsi que l artiste, la date et l heure</h2>';
    echo '<table>';
    connect_db();
    $reponse = $conn->query('SELECT * FROM shows ORDER BY title ASC');
    while ($donnees = $reponse->fetch()){
        echo '<tr>';
        echo '<td>' . $donnees['title'] . " par " . $donnees['performer'] . ", le ".$donnees['date'] . " " . $donnees['startTime']. "</td>";
        echo '</tr>';
    }
    $reponse->closeCursor();
    echo '</table>';
}
else if($_GET['question'] === 'ex7'){
    echo '<h2>Afficher tous les clients</h2>';
    echo '<table><tr>
            <th>ID</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>DOB</th>
            <th>Carte de fidélité</th>
            </tr>';
    connect_db();
    $reponse = $conn->query("SELECT clients.id, clients.lastName, 
                clients.firstName, clients.birthDate, cards.cardTypesId, 
                cards.cardNumber from clients LEFT JOIN cards ON 
                clients.cardNumber = cards.cardNumber ORDER BY id ASC");
    while ($donnees = $reponse->fetch()){
        echo '<tr>';
        echo '<td>' . $donnees['id'] . "</td>";
        echo '<td>' . $donnees['lastName'] . "</td>";
        echo '<td>' . $donnees['firstName'] . "</td>";
        echo '<td>' . $donnees['birthDate'] . "</td>";

        if ($donnees['cardTypesId']==1) {
        echo '<td>' . 'OUI' . ' / '. $donnees['cardNumber'] . "</td>";
        }
        else{
             echo '<td>' . 'No' . "</td>";;
        }
    }
    $reponse->closeCursor();
    echo '</table>';
}
else{
    echo "No records matching your query were found.";
}

