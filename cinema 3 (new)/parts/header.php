<!DOCTYPE html>
<html>
<head>
    <title>CINEMA</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="clear"></div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary stickyHeader">
      <a class="navbar-brand" href="#">CINEMA</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link active" href="index.php?page=content&question=ex1">Exercise 1 <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=content&question=ex2">Exercise 2</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=content&question=ex3">Exercise 3</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=content&question=ex4">Exercise 4</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=content&question=ex5">Exercise 5</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=content&question=ex6">Exercise 6</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=content&question=ex7">Exercise 7</a>
          </li>
        </ul>
      </div>
    </nav>
</body>
</html>